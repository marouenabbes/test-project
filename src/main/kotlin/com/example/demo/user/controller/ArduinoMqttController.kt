package com.example.demo.user.controller

import com.example.demo.user.service.ArduinoMqttService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/arduino")
class ArduinoMqttController(val arduinoMqttService: ArduinoMqttService) {


    @GetMapping("/writeTOArduino/{data}")
    fun writeToArduino(@PathVariable data: String) {
        arduinoMqttService.sendMessage(data)
    }

    @GetMapping("/readToArduino")
    fun readFromArduino(): String {
        return arduinoMqttService.readFromMqtt()
    }
    @GetMapping("/welcome")
    fun welcome(): String {
        return "hello arduino"
    }
}