package com.example.demo.user.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.UUID

@Document(collection = "user_machine")
data class UserMachine (
    @Id
    var id: UUID = UUID.randomUUID(),
    var userName: String,
    var userRole: String,
)
