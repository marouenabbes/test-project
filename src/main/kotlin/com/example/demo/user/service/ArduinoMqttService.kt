package com.example.demo.user.service

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.springframework.stereotype.Service

@Service
class ArduinoMqttService {
    private val mqttClient = MqttClient("tcp://localhost:1883", MqttClient.generateClientId())

    init {
        mqttClient.connect()
    }

    fun sendMessage(data: String) {
        val message = MqttMessage(data.toByteArray())
        mqttClient.publish("my/arduino/topic", message)
    }

    fun readFromMqtt(): String {
        var result = ""
        mqttClient.setCallback(object : MqttCallback {
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                result = String(message?.payload ?: byteArrayOf())
            }

            override fun connectionLost(cause: Throwable?) {
                println("Connection lost: ${cause?.message}")
            }

            override fun deliveryComplete(token: IMqttDeliveryToken?) {
            }
        })
        mqttClient.subscribe("my/spring/topic")
        return result
    }
}